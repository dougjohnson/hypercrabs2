module Commodore
  autoload :Etcd, 'commodore/etcd'
  autoload :Configuration, 'commodore/configuration'
  autoload :Application, 'commodore/application'
  autoload :CommandBuilder, 'commodore/command_builder'
end
