module Commodore
  class CommandBuilder

    attr_reader :app, :config

    def initialize(app, config, options)
      @app = app
      @convoy_id = options[:convoy]
      @cluster = options[:cluster]
      @config = config
    end

    def name(options = {})
      mode = options[:mode]
      scale = options[:scale]
      container_name(app.name, mode, scale)
    end

    def start(options = {})
      name = name(options)
      docker = []

      run_daemon(app, docker)
      run_name(name, docker)
      
      app_links(app, docker)
      dep_links(app, docker)
      env_flags(app, docker)
      proxy_flags(app, options[:mode], docker)
      volumes_flags(app, nil, docker)
      docker_flags(app, docker)

      docker_image(app, docker)
      container_run(options, docker)
      docker.compact
    end

    private

    def run_daemon(app, docker)
      docker << "docker run -d"
    end
    
    def run_name(name, docker)
      docker << "--name #{name}"
    end

    def docker_image(app, docker)
      docker << app.image
    end

    def container_run(options, docker)
        docker << options[:command]
    end
  
    def container_name(app, mode = nil, scale=nil)
      parts = []
      parts << @convoy_id
      parts << app
      parts << mode
      parts << scale
      parts.compact.join('_')
    end

    def app_fqdn(app)
      parts = []
      parts << @convoy_id
      parts << app
      parts << @cluster
      parts.compact.join('-')
    end

    def app_env_addr(app)
      "#{app.upcase}_HOST_ADDR"
    end

    def app_links(app, docker)
      links = app.linked_apps(config)
      links.each do |link|
        fqdn = app_fqdn(link)
        env = app_env_addr(link)
        docker << "-e \"#{env}=https://#{fqdn}\""
        docker << "--link=host_proxy:#{fqdn}"
      end
    end

  def dep_links(app, docker)
    links = app.dependencies(config)
    links.each do |link|
      name = container_name(link)
      docker << "--link=#{name}:#{link}"
    end
  end

  def proxy_flags(app, mode, docker)
    if app.proxy_to?(mode)
      fqdn = app_fqdn(app.name)
      port = app.proxy_port(mode)
      docker << "-e \"VIRTUAL_HOST=#{fqdn}\""
      docker << "-e \"VIRTUAL_PORT=#{port}\""
    end
  end

  def volumes_flags(app, mode, docker)
    app.volumes_from.each do |v|
      docker << "--volumes-from=#{v}"
    end
  end

  def docker_flags(app,docker)
    docker << config.docker_args(app.name)
  end

  def env_flags(app, docker)
    if app.env_var?
      docker << "-e \"#{app.env_var}=#{config.environment}\""
    end
  end

  end
end
