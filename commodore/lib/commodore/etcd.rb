require 'json'

module Commodore
  class Etcd
    Response = Struct.new(:etcd_index, :action, :node, :prevNode)

    class Node
      attr_reader :createdIndex, :modifiedIndex, :value, :key

      def initialize(data)
        data ||= {}
        parse_data data, :createdIndex, :modifiedIndex,
                         :value, :key
      end

      private

      def parse_data(data, *attrs)
        attrs.each do |attr|
          instance_variable_set("@#{attr}", data[attr.to_s])
        end
      end

    end

    def self.client(options)
      new(options)
    end

    def initialize(options)
      @options = options
    end

    def get(key, params = {})
      response = make_get(key, params)
      json = JSON.parse(response.body)
      Response.new response['X-Etcd-Index'].to_i,
                   json['action'],
                   Node.new(json['node']),
                   Node.new(json['prevNode'])
    end

    def watch(key, options = {})
      options[:wait] = true
      get(key, options)
    end

    private

    def make_uri(path)
      host = @options[:host]
      port = @options[:port] || 4001
      uri = URI("http://#{host}:#{port}")
      uri.path = "/v2/keys" + path
      uri
    end

    def make_query(params)
      params.map do |k, v|
        "#{k}=#{v}"
      end.join '&'
    end

    def make_get(path, params)
      uri = make_uri(path)
      if params.length > 0
        uri.query = make_query(params)
      end
      Net::HTTP.get_response(uri)
    end
  end
end
