require 'spec_helper'

describe Commodore::CommandBuilder do
  let(:app) do
    app = double(:name => 'the_app',
                 :image => 'the_image',
                 :linked_apps => [],
                 :dependencies => [],
                 :env_var? => false,
                 :proxy_to? => false,
                 :volumes_from => [])
    app
  end

  let(:config) do
    double :environment => "config_env",
           :docker_args => nil
  end

  subject do
    described_class.new app, config, :convoy => 'convoy_id',
                                     :cluster => 'the-cluster.com'
  end

  describe "#name" do
    let(:name) { subject.name }

    it "uses the given name" do
      expect(name).to eq "convoy_id_the_app"
    end

    context "with a given mode" do
      let(:name) { subject.name :mode => 'the_mode' }

      it "adds the mode to the name" do
        expect(name).to eq "convoy_id_the_app_the_mode"
      end
    end

    context "with a given scale" do
      let(:name) { subject.name :scale => 4 }

      it "adds the scale to the name" do
        expect(name).to eq "convoy_id_the_app_4"
      end
    end
  end

  describe "#start" do
    let(:cmd) { subject.start }

    it "runs the container in daemon mode" do
      expect(cmd.first).to eq "docker run -d"
    end

    it "starts the appropriate image" do
      expect(cmd.last).to eq "the_image"
    end

    context "when there's a given command" do
      let(:cmd) { subject.start :command => "the command to run" }

      it "starts the image with the command" do
        expect(cmd[-2]).to eq "the_image"
        expect(cmd[-1]).to eq "the command to run"
      end
    end

    describe "Container Name" do
      it "gives the container a name" do
        expect(cmd).to include "--name convoy_id_the_app"
      end
    end

    describe "Application Links" do
      before :each do
        expect(app).to receive(:linked_apps).with(config).and_return(['app1', 'app2'])
      end

      it "sets an environment variable for the host" do
        expect(cmd).to include "-e \"APP1_HOST_ADDR=https://convoy_id-app1-the-cluster.com\""
        expect(cmd).to include "-e \"APP2_HOST_ADDR=https://convoy_id-app2-the-cluster.com\""
      end

      it "links to the host proxy" do
        expect(cmd).to include "--link=host_proxy:convoy_id-app1-the-cluster.com"
        expect(cmd).to include "--link=host_proxy:convoy_id-app2-the-cluster.com"
      end
    end

    describe "Dependency Links" do
      before :each do
        expect(app).to receive(:dependencies).with(config).and_return(['dep1', 'dep2'])
      end

      it "links to the container" do
        expect(cmd).to include "--link=convoy_id_dep1:dep1"
        expect(cmd).to include "--link=convoy_id_dep2:dep2"
      end
    end

    describe "Environment" do
      context "when there's an env_variable" do
        before :each do
          expect(app).to receive(:env_var?) { true }
          expect(app).to receive(:env_var) { "THE_ENV_VAR" }
        end

        it "sets an environment variable with the config's environment" do
          expect(cmd).to include "-e \"THE_ENV_VAR=config_env\""
        end
      end

      context "when there's no env variable" do
        it "sets no environment variable" do
          expect(cmd.detect { |c| c.match /config_env/ }).to be_nil
        end
      end
    end

    describe "Proxy" do
      context "with a proxy setting" do
        before :each do 
          expect(app).to receive(:proxy_to?) { true }
          expect(app).to receive(:proxy_port) { 1234 }
        end

        it "adds a VIRTUAL_HOST variable for the application" do
          expect(cmd).to include "-e \"VIRTUAL_HOST=convoy_id-the_app-the-cluster.com\""
        end

        it "adds a VIRTUAL_PORT variable for the application" do
          expect(cmd).to include "-e \"VIRTUAL_PORT=1234\""
        end
      end

      context "with no proxy" do
        it "adds no proxy variables" do
          expect(cmd.detect { |c| c.match /VIRTUAL/ }).to be_nil
        end
      end
    end

    describe "Volumes" do
      before :each do
        expect(app).to receive(:volumes_from).and_return(['fromvol1', 'fromvol2'])
      end

      it "adds any given volumes from" do
        expect(cmd).to include "--volumes-from=fromvol1"
        expect(cmd).to include "--volumes-from=fromvol2"
      end
    end

    describe "Docker Flags" do
      before :each do
        expect(config).to receive(:docker_args).and_return('-some flags for docker')
      end

      it "adds them to the command" do
        expect(cmd).to include "-some flags for docker"
      end
    end

  end
end
