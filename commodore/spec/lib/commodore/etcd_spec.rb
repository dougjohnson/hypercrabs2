require 'spec_helper'

describe Commodore::Etcd do
  let(:options) do
    {:host => 'etcdhost', :port => '1234'}
  end

  let(:example_record) do
    <<-JSON
    {
      "action":"The Action",
      "node": {
           "createdIndex": 1,
           "key": "/akey",
           "modifiedIndex": 2,
           "value": "New Value"
        },
      "prevNode": {
            "createdIndex": 1,
            "key": "/akey",
            "value": "Prev Value",
            "modifiedIndex": 3
        }
    }
    JSON
  end

  let(:example_headers) do
    {
      "X-Etcd-Index" => "999"
    }
  end

  subject { described_class.client options }

  before :each do
    expect(Net::HTTP).to receive(:get_response) do |uri|
      @uri = uri

      response = example_headers
      allow(response).to receive(:body) { example_record }
      response
    end
  end

  describe "#get" do

    it "fetches a response from the given key" do
      record = subject.get('/some/key')

      expect(@uri.to_s).to eq 'http://etcdhost:1234/v2/keys/some/key'

      expect(record.etcd_index).to be 999
      expect(record.action).to eq "The Action"
      expect(record.node.createdIndex).to eq 1
      expect(record.node.modifiedIndex).to eq 2
      expect(record.node.value).to eq "New Value"
      expect(record.prevNode.value).to eq "Prev Value"
    end
  end

  describe "#watch" do
    it "fetches with a wait" do
      record = subject.watch('/some/key')
      expect(@uri.to_s).to eq 'http://etcdhost:1234/v2/keys/some/key?wait=true'
    end
  end

end
