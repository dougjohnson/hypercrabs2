require 'spec_helper'

describe Commodore::Configuration do

  describe ".from_file" do
    it "loads from the file" do
      expect(YAML).to receive(:load_file).with('the_file.yml').and_return('YAML')
      expect(described_class).to receive(:new).with('YAML')

      described_class.from_file('the_file.yml')
    end
  end

  let(:yaml) do
    <<-YAML
    apps:
      app1:
      app2:
    environments:
      one:
        dependencies:
          dep1:
          dep2:
        pre:
          - pretask1
          - pretask2
        post:
        docker:
          app1: --some argument
      two:
        dependencies:
          twodep1:
          twodep2:
        post:
          - posttask1
          - posttask2
        docker:
          app1: --different argument
    YAML
  end

  subject { described_class.from_string(yaml) }

  describe "Applications" do

    describe "#apps" do
      it "yields an application for each application in the config" do
        apps = []
        subject.apps do |app|
          apps << app
        end

        names = apps.map &:name

        expect(names).to include 'app1'
        expect(names).to include 'app2'
      end
    end

    describe "#applications" do
      it "returns array of app names" do
        expect(subject.applications).to eq ['app1', 'app2']
      end
    end

    describe "#find_app" do
      it "finds the an application by the given name" do
        apps = []
        subject.apps { |a| apps << a }

        expect(subject.find_app(apps[1].name)).to equal apps[1]
      end
    end

  end

  describe "Dependencies" do
    before :each do
      subject.set_env('one')
    end

    describe "#dependencies" do
      it "yields and application for each defined dependency in the config" do
        apps = []
        subject.dependencies { |a| apps << a }

        names = apps.map &:name

        expect(names).to include 'dep1'
        expect(names).to include 'dep2'
      end

      it "uses the given environment" do
        subject.set_env('two')
        apps = []
        subject.dependencies { |a| apps << a.name }

        expect(apps).to eq ['twodep1', 'twodep2']
      end
    end
  end

  describe "Tasks" do
    before :each do
      subject.set_env('one')
    end

    describe "#pre" do
      it "yields commands to run prior to app container launch" do
        cmds = []
        subject.pre { |c| cmds << c }

        expect(cmds).to eq ['pretask1', 'pretask2']
      end

      it "uses the environment" do
        subject.set_env('two')
        cmds = []
        subject.pre { |c| cmds << c }
        expect(cmds).to be_empty
      end
    end

    describe "#post" do
      it "yields commands to run after to app container launch" do
        subject.set_env('two')
        cmds = []
        subject.post { |c| cmds << c }

        expect(cmds).to eq ['posttask1', 'posttask2']
      end

      it "uses the environment" do
        cmds = []
        subject.post { |c| cmds << c }
        expect(cmds).to be_empty
      end
    end
  end

  describe "Docker Arguments" do
    before :each do
      subject.set_env('one')
    end

    it "gives the docker args from the environment for given app" do
      expect(subject.docker_args('app1')).to eq "--some argument"
      subject.set_env('two')
      expect(subject.docker_args('app1')).to eq "--different argument"
    end
  end
end
