boxid=(aws ec2 run-instances \
    --image-id ami-36832441 \
    --instance-type r3.xlarge \
    --key-name maersk \
    --subnet-id subnet-c3696785 \
    --associate-public-ip-address \
    --security-group-ids sg-fdc46c98 \
    --block-device-mappings "[{\"DeviceName\": \"/dev/xvdb\",\"VirtualName\":\"ephemeral0\"},{\"DeviceName\" : \"/dev/xvdf\",\"Ebs\" : { \"SnapshotId\" : \"snap-b519a34f\", \"VolumeSize\" : 300, \"DeleteOnTermination\" : true, \"VolumeType\": \"gp2\"}}]" \
    --user-data file://./cloud-config.yaml \
    | grep InstanceId | awk '{print $4}')

#    --block-device-mappings "[{\"DeviceName\": \"/dev/xvdb\",\"VirtualName\":\"ephemeral0\"}]" \

aws ec2 describe-instances --instance-id=$boxid \
    | grep PublicIpAddress \
    | awk '{print $4}'

aws ec2 create-tags \
     --resources $boxid \
     --tags Key=Name,Value=navyhost1 Key=Project,Value=maersk
