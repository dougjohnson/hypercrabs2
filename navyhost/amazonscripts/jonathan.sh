aws ec2 run-instances \
    --image-id ami-cec912b9 \
    --instance-type r3.large \
    --key-name maersk \
    --subnet-id subnet-c3696785 \
    --associate-public-ip-address \
    --security-group-ids sg-fdc46c98 \
    --block-device-mappings "[{\"DeviceName\": \"/dev/xvdb\",\"VirtualName\":\"ephemeral0\"},{\"DeviceName\" : \"/dev/xvdf\",\"Ebs\" : { \"VolumeSize\" : 50, \"DeleteOnTermination\" : false, \"VolumeType\": \"gp2\"}}]" \
    --user-data file://./jonathan-cloud-config.yaml \
    | grep InstanceId | awk '{print $4}'


aws ec2 describe-instances --instance-id=i-f92f0ebb \
    | grep PublicIpAddress \
    | awk '{print $4}'

aws ec2 create-tags \
     --resources i-f92f0ebb \
     --tags Key=Name,Value=navyhost1 Key=Project,Value=maersk
