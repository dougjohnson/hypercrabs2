#!/bin/bash -e
sed -i '.orig' "s#discovery: .*#discovery: $(curl -s https://discovery.etcd.io/new 2>&1)#" cloud-config*
rm *.orig
