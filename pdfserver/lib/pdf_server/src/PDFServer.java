package pdfserver;

import java.net.*;
import java.io.*;

public class PDFServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        boolean listening = true;

        try {
            serverSocket = new ServerSocket();
            InetAddress inetAddress = InetAddress.getByName("127.0.0.1");
            InetSocketAddress inetSocketAddress = 
                new InetSocketAddress(inetAddress, Integer.parseInt(args[0]));
            serverSocket.bind(inetSocketAddress);
            System.out.println("");
            System.out.println("PDFServer listening on port " + args[0]);
            System.out.println("(hit enter)");
        } catch (IOException e) {
            System.err.println("Could not listen on port: " + args[0] + ".");
            System.exit(-1);
        }

        while (listening)
        new PDFServerThread(serverSocket.accept()).start();

        serverSocket.close();
    }
}

