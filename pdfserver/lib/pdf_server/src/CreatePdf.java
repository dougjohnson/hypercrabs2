package pdfserver;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author eamon
 */
public class CreatePdf {

    public byte[] makePdf(String htmlInput) {

        byte[] pdfOut = null;
        
        try {
            Tidy tidy = new org.w3c.tidy.Tidy();
            tidy.setQuoteAmpersand(true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            tidy.parse(new StringReader(htmlInput), ps);
            ITextRenderer renderer = new ITextRenderer();
            String tidyHtmlString = baos.toString("UTF8");
	    ps.close();
	    baos.close();
            renderer.setDocumentFromString(tidyHtmlString);
            //This line is important
            renderer.layout();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            renderer.createPDF(byteArrayOutputStream, true);

            pdfOut = byteArrayOutputStream.toByteArray();

            byteArrayOutputStream.close();

        } catch (IOException ex) {
            Logger.getLogger(CreatePdf.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(CreatePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
	return pdfOut;
    }
}

