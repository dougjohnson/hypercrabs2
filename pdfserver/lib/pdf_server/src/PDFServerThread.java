package pdfserver;

import java.net.*;
import java.io.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;

public class PDFServerThread extends Thread {

    private Socket socket = null;

    public PDFServerThread(Socket socket) {
        super("PDFServerThread");
        this.socket = socket;
    }

    public void run() {

        try {
	    OutputStream sout = socket.getOutputStream();
			
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                    socket.getInputStream()));

            String inputLine;

	    StringBuffer strBuffer = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
		strBuffer.append(inputLine);;
            }

	   CreatePdf pdfCreator = new CreatePdf();
	   byte[] pdf = pdfCreator.makePdf(strBuffer.toString());
           sout.write(pdf);
	   sout.close();
           socket.close();

        } catch (Exception e1) {
            e1.printStackTrace();
        } 
    }
}
